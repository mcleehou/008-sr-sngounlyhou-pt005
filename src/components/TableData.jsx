// import { Button } from 'bootstrap'
import React from 'react'
import { Container, Table,Button } from 'react-bootstrap'

function TableData(props) {


    // console.log("Props",props.items);
    let temp = props.items.filter(item => {
        return item.amount > 0
    })
    let subtotal = 0
    for(let i = 0 ; i< props.items.length ; i++){
        subtotal += props.items[i].total
    }
    let discount = subtotal*(5/100)
    let grandtotal = subtotal - discount

    
    return (
        <Container>
            <h1>Table</h1>
            <Button variant="warning" onClick={props.onClear}>Clear</Button> <> </>
            <Button variant="primary">{temp.length}</Button>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Quantity</th>
                        <th>Unit Price</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                {temp.map((item, index) => (
                    <tbody>
                        <tr>
                            <td>{index+1}</td>
                            <td>{item.title}</td>
                            <td>{item.amount}</td>
                            <td>{item.price} $</td>
                            <td>{item.total} $</td>
                        </tr>

                    </tbody>
                ))}
                
                <tbody>
                    <tr>
                        <td colSpan={4} style={{ textAlign: 'right' }}>Sub Total</td>
                        <td>{subtotal} $</td>
                    </tr>
                    <tr>
                        <td colSpan={4} style={{ textAlign: 'right' }}>Discount 5%</td>
                        <td>{discount} $</td>
                    </tr>
                    <tr>
                        <td colSpan={4} style={{ textAlign: 'right' }}>Grand Total</td>
                        <td>{grandtotal} $</td>
                    </tr>
                </tbody>
                
            </Table>
        </Container>
    )
}

export default TableData
